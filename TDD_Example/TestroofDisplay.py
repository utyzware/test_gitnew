

import unittest
import roofDisplay





class Test_Display_Signal_Value (unittest.TestCase):



    def test_RoofDisplay_func(self):
        self.assertEqual("please fasten your seatbelt",  roofDisplay.displayMessage(1))
        self.assertEqual("Next stop Mellanvagen",  roofDisplay.displayMessage(2))
        self.assertEqual("Next stop Saabvagen",  roofDisplay.displayMessage(3))
        self.assertEqual("Next stop Nevs Main Entrance",  roofDisplay.displayMessage(4))



if __name__ == '__main__':
    unittest.main()
