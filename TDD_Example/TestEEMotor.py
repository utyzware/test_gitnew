# TestEEMotor.py

import unittest
import EEMotor


class KnowSignalValues(unittest.TestCase):
	
	def test_EEMotor_Func_LeftIn(self):
		result = EEMotor.EEMotorLeft_Func(1)
		expected = "Fold in"
		self.assertEqual(expected, result)

	def test_EEMotor_Func_LeftOut(self):
		result = EEMotor.EEMotorLeft_Func(2)
		expected = "Fold out"
		self.assertEqual(expected, result)

	def test_EEMotor_Func_Right(self):
		self.assertEqual("Fold in", EEMotor.EEMotorRight_Func(1))
		self.assertEqual("Fold out", EEMotor.EEMotorRight_Func(2))
		self.assertEqual("No Command", EEMotor.EEMotorRight_Func(3))
		self.assertEqual("No Command", EEMotor.EEMotorRight_Func('None'))

	def test_EEMotor_Func_LnR(self):
		self.assertEqual("Fold in", EEMotor.EEMotorLnR_Func(1, 1))
		self.assertEqual("Fold out", EEMotor.EEMotorLnR_Func(2, 2))
		self.assertEqual("LIn ROut", EEMotor.EEMotorLnR_Func(1, 2))
		self.assertEqual("LOut RIn", EEMotor.EEMotorLnR_Func(2, 1))

if __name__ == '__main__':
	unittest.main()