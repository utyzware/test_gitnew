
#EEMotor(int)
#input: EECommand (1, 2) [int]
#output: EEStatus ("Fold in", "Fold out") [string]
def EEMotorLeft_Func(EECommand):
    if EECommand == 1:
        EEStatus = "Fold in"
    elif EECommand == 2:
        EEStatus = "Fold out"
    return EEStatus

def EEMotorRight_Func(EECommand):
    if EECommand == 1:
        EEStatus = "Fold in"
    elif EECommand == 2:
        EEStatus = "Fold out"
    else:
        EEStatus = "No Command"
    return EEStatus

def EEMotorLnR_Func(EECommandLeft,EECommandRight):
    if EECommandLeft == 1 and EECommandRight == 1:
        EEStatus = "Fold in"
    elif EECommandLeft == 2 and EECommandRight == 2:
        EEStatus = "Fold out"
    elif EECommandLeft == 1 and EECommandRight == 2:
        EEStatus = "LIn ROut"
    elif EECommandLeft == 2 and EECommandRight == 1:
        EEStatus = "LOut RIn"


    return EEStatus