#RoofDisplay.py
int_cases = {1: "Please fasten your seatbelt", 2: "Next stop Mellanvagen",
                     3: "Next stop Saabvagen", 4: "Next stop Nevs Main Entrance"}
def displayFunc(sigValue):
    # Check if the input is an int, if not raise exception
    type_set = (int)
    if isinstance(sigValue, type_set):
        # Check if the input is within the range, if not raise exception
        if 0 < sigValue < 5:
            return int_cases[sigValue]
        else:
            raise ValueError
    else:
        raise ValueError