import unittest
import RoofDisplay


class KnownSignalValue(unittest.TestCase):
    def test_roofDisplay_Int(self):
#        int_cases = {1: "Please fasten your seatbelt", 2: "Next stop Mellanvagen",
#                     3: "Next stop Saabvagen", 4: "Next stop Nevs Main Entrance"}
        for sigValue, expected in RoofDisplay.int_cases.items():
            self.assertEqual(expected, RoofDisplay.displayFunc(sigValue))

#        self.assertEqual("Please fasten your seatbelt", RoofDisplay.roofDisplay(1))
#        self.assertEqual("Next stop ", RoofDisplay.roofDisplay(2))
#        self.assertEqual("Next stop ", RoofDisplay.roofDisplay(3))

    def test_roofDisplay_notInt(self):
        exceptions = {10, 1.23, "String", None}
        for i in exceptions:
            self.assertRaises(ValueError, RoofDisplay.displayFunc, i)
#        self.assertRaises(ValueError, RoofDisplay.displayFunc, "String")
#        self.assertRaises(ValueError, RoofDisplay.displayFunc, None)


if __name__ == '__main__':
    unittest.main()
